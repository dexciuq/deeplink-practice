package com.dexciuq.deeplink_practice

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.dexciuq.deeplink_practice.databinding.ActivityDetailsBinding

class DetailsActivity : AppCompatActivity() {

    private val binding by lazy { ActivityDetailsBinding.inflate(layoutInflater) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        intent.data?.let {
            val btnText = it.getQueryParameter("btn")
            binding.button.text = btnText ?: getString(R.string.empty)
        }
    }
}